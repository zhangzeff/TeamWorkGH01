﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH.Utils
{
    class IsAdmin
    {
        public static bool IsAdminstrator(string UserName, string ProjectOwner)
        {
            return UserName==ProjectOwner;
        }
    }
}
