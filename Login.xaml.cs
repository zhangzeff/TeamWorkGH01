﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        public GitHubServiceClient client;
        public Login()
        {
            InitializeComponent();
            System.Threading.Thread.Sleep(2000);
            client = new GitHubServiceClient();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Register register = new Register();
            register.Show();
            this.Close();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            if (UserName.Text == "" || Password.Password == "")
            {
                MessageBox.Show("用户名或密码不能为空");
            }
            else
            {
                UserDTO user = client.FindUserByUserName(UserName.Text);
                if (user == null)
                {
                    MessageBox.Show("用户不存在");
                }
                else if (!user.Password.Equals(Password.Password))
                {
                    MessageBox.Show("密码错误\n" + user.Password + "caonima" + '\n' + Password.Password + '\n' + user.Password.Equals(Password.Password).ToString());
                }
                else
                {
                    MainWindow mainWindow = new MainWindow(user.UserName);
                    mainWindow.Show();
                    this.Close();
                }
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
