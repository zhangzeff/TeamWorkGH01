﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        string currentPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "AdvertisingImages";
        public List<Photo> photos = new List<Photo>();
        GitHubServiceClient client;
        UserDTO user;

        public MainWindow(string UserName)
        {
            InitializeComponent();
            //广告位
            GetAllImagePath(currentPath);
            listBox1.ItemsSource = photos;
            client = new GitHubServiceClient();
            this.user = client.FindUserByUserName(UserName);

            userName.Text = this.user.UserName;


            foreach (var item in client.GetAllProjectsByUserName(user.UserName))
            {
                string s = "ProjectName:" + item.ProjectName + '\t' + item.ProjectId.ToString();
                Repositories.Items.Add(s);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        public class Photo
        {
            public string FullPath { get; set; }
        }

        public void GetAllImagePath(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] files = di.GetFiles("*.*", SearchOption.AllDirectories);
            if (files != null && files.Length > 0)
            {
                foreach (var file in files)
                {
                    if (file.Extension == (".jpg") ||
                        file.Extension == (".png") ||
                        file.Extension == (".bmp") ||
                        file.Extension == (".gif"))
                    {
                        photos.Add(new Photo()
                        {
                            FullPath = file.FullName
                        });
                    }
                }
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = (listBox1.SelectedIndex + 1) % listBox1.Items.Count;
                listBox1.ScrollIntoView(listBox1.Items[listBox1.SelectedIndex]);
            }
        }

        private void listBox1_Loaded(object sender, RoutedEventArgs e)
        {
            timer.Interval = TimeSpan.FromMilliseconds(2500);
            timer.Tick += new EventHandler(timer_Tick);
            listBox1.SelectedIndex = 0;
            timer.Start();
        }

        private void listBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            int index = listBox1.SelectedIndex;
            switch (index)
            {
                case 0: Process.Start("https://gitee.com/zhangzeff/Group10WcfGitHub"); break;
                case 1: Process.Start("https://www.bilibili.com/video/BV1bp4y197TX");break;
                case 2: Process.Start("https://shop250494815.taobao.com/?spm=a211vu.server-web-home.sellercard.15.64f02d585tYQiJ"); break;
                case 3: Process.Start("https://zhangzef.com/"); break;
                default:
                    break;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                Login login = new Login();
                int itemName = comboBox1.SelectedIndex;
                switch (itemName)
                {
                    case 1: login.Show(); this.Close(); break;
                    default:
                        break;
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Marketplace marketplace = new Marketplace(user.UserName);
            marketplace.Show();
            this.Close();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void NewProject_Click(object sender, RoutedEventArgs e)
        {
            NewProjectWindow newProject = new NewProjectWindow(user.UserName, this);
            newProject.Show();
        }

        private void Repositories_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string[] s = Repositories.SelectedItem.ToString().Split('\t');
            if (s[1] != "ProjectId")
            {
                ProjectDTO project = client.FindProjectById(Convert.ToInt32(s[1]));
                ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
                projectWindow.Show();
                this.Close();
            }
        }
    }
}
