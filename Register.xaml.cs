﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// Register.xaml 的交互逻辑
    /// </summary>
    public partial class Register : Window
    {
        public GitHubServiceClient client;
        public Register()
        {
            InitializeComponent();
            client = new GitHubServiceClient();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            string userName = UserName.Text;
            string password = Password.Password;
            string rePassword = RepeatPassword.Password;

            if (userName=="" || password=="")
            {
                MessageBox.Show("用户名或密码不能为空");
            }
            else if (rePassword != password)
            {
                MessageBox.Show("两次密码不一致");
            }
            else if (client.FindUserByUserName(userName) != null)
            {
                MessageBox.Show("用户名已存在");
            }
            else if (userName.Length<3 || userName.Length>10 || password.Length<3 || password.Length>10)
            {
                MessageBox.Show("用户名或密码的长度至少为3，至多为10");
            }
            else
            {
                client.AddUser(userName, password);
                MessageBox.Show("注册成功");
                Login login = new Login();
                login.Show();
                this.Close();
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
